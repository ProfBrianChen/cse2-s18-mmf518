//Matthew Firestone
//CSE2 Lab02
//02 Feb 2018
/* Cyclometer program - Prints the following information for two given bike trips:
   number of minutes for each trip
   number of counts for each trip
   distance of each trip in miles
   distance for the two trips combined
*/

public class Cyclometer {
  
    	// main method 
   	public static void main(String[] args) {
      
      //input data      
      int secsTrip1=480;     //duration of 1st trip in seconds 
      int secsTrip2=3220;    //duration of 2nd trip in seconds
      int countsTrip1=1561;  //number of wheel rotations in 1st trip 
      int countsTrip2=9037;  //number of wheel rotations in 2nd trip
      
      //conversions constants and output data
      double wheelDiameter=27.0;      //diameter of bike wheel in inches
  	  double PI=3.14159;              //value of pi 
  	  double feetPerMile=5280;           //number of feet per one mile for conversion 
  	  double inchesPerFoot=12;           //number of inches per one foot for conversion
  	  double secondsPerMinute=60;        //number of seconds per one minute for conversion
	    double distanceTrip1, distanceTrip2, totalDistance;  //variables to hold calculated values of distance
      
      //print input data collected by cyclometer
      System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
	    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
      
      //calculate output data
      distanceTrip1=countsTrip1*wheelDiameter*PI; //Calculate distance of trip 1 in inches (for each count, a rotation of the wheel travels the diameter in inches times PI)
	    distanceTrip1/=inchesPerFoot*feetPerMile; //Convert distance of trip 1 to miles
	    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //Calculate distance of trip 2 in feet
	    totalDistance=distanceTrip1+distanceTrip2; //Calculate total distance
      
      //print output data.
      System.out.println("Trip 1 was "+distanceTrip1+" miles");
	    System.out.println("Trip 2 was "+distanceTrip2+" miles");
	    System.out.println("The total distance was "+totalDistance+" miles");
      

	}  //end of main method   
} //end of Cyclometer class

