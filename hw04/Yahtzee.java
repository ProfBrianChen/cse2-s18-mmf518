//Matthew Firestone
//CSE2 hw04
//20 Feb 2018
/* Yahtzee program: 
   Plays Yahtzee in one of two ways: 
   User chooses 5 dice or 5 are randomly generated
   Displays subtotals and final score
*/ 
import java.util.Scanner;

public class Yahtzee {
  
    //main method 
   	public static void main(String[] args) {
      
      Scanner myScanner = new Scanner(System.in); //declare scanner object
      
      //declare and initialize variables to be used in yahtzee
      int die1=0, die2=0, die3=0, die4=0, die5=0; //will hold face value of each die 
      int upperSectionSubtotal=0, upperSectionBonus=0, upperSectionTotal=0, lowerSectionTotal=0, grandTotal=0; //total scores to be printed
      int userDice=0; //5 digit number representing the five dice
      String userDiceString=""; //string version of userDice (the 5 digits representing the dice)
      int ones=0, twos=0, threes=0, fours=0, fives=0, sixes=0; //counts of how many of each number were rolled
      int onesScore=0, twosScore=0, threesScore=0, foursScore=0, fivesScore=0, sixesScore=0, bonusScore=0; //results for upper section score components
      int threeKind=0, fourKind=0, fullHouse=0, smStraight=0, lgStraight=0, yahtzee=0, chance=0; //results for lower section score components
      
      /*asks the user whether they would like to enter their own dice or randomly generate a roll.
        user enters 1 to input, or 2 to randomize*/
      System.out.println("Would you like to enter your own 5 dice values, or randomly roll the dice?\nEnter 1 to input your dice, or 2 for a random roll.");
      int rollType = myScanner.nextInt();   //read user input of 1 or 2
      
      switch ( rollType ) { //different methods of assigning values to dice (random or from user input)
        case 1: //ask user to enter 5 digits, store them as the dice values
          System.out.println("Please enter 5 digits to represent the values of your 5 dice, such as \"12345\": ");
          userDice = myScanner.nextInt();
          userDiceString = Integer.toString(userDice);
          if ( userDiceString.length()==5 ) {//checks that there are 5 values
            die5 = userDice % 10; //determine 5th digit entered
            die4 = (userDice/10) % 10; //determine 4th digit entered
            die3 = (userDice/100) % 10; //determine 3rd digit entered
            die2 = (userDice/1000) % 10; //determine 2nd digit entered
            die1 = (userDice/10000); //determine 1st digit entered
          }
        break;
        case 2: //randomly assign number from 1-6 to each of the 5 dice
          die1 = (int) ((Math.random()*6)+1);
          die2 = (int) ((Math.random()*6)+1);
          die3 = (int) ((Math.random()*6)+1);
          die4 = (int) ((Math.random()*6)+1);
          die5 = (int) ((Math.random()*6)+1);
          userDiceString = Integer.toString(die1) + Integer.toString(die2) + Integer.toString(die3) + Integer.toString(die4) + Integer.toString(die5);
          System.out.println("Your dice are: " + userDiceString); //shows the user what was rolled 
        break;
        default: //if the user did not enter 1 or 2, dice values will remain zero and it will display the Invalid message. 
        break;
      }//end switch (rollType)      
      
      /*validate that the dice amounts are between 1 and 6, and that there are 5 values 
        if not, display Invalid message*/
      if ( (userDiceString.length()==5) && die1>=1 && die1<=6 && die2>=1 && die2<=6 && die3>=1 && die3<=6 && die4>=1 && die4<=6 && die5>=1 && die5<=6 ) {
        //determine counts of each number that were rolled by incrementing
        switch ( die1 ) { //adds result of die 1 to the appropriate count 
          case 1:
            ones++;
          break;  
          case 2:
            twos++;
          break;
          case 3:
            threes++;
          break;
          case 4:
            fours++;
          break;
          case 5:
            fives++;
          break;
          case 6:
            sixes++;
          break;
        }//end switch die1 
        switch ( die2 ) { //adds result of die 2 to the appropriate count 
          case 1:
            ones++;
          break;  
          case 2:
            twos++;
          break;
          case 3:
            threes++;
          break;
          case 4:
            fours++;
          break;
          case 5:
            fives++;
          break;
          case 6:
            sixes++;
          break;
        }//end switch die2       
        switch ( die3 ) { //adds result of die 3 to the appropriate count 
          case 1:
            ones++;
          break;  
          case 2:
            twos++;
          break;
          case 3:
            threes++;
          break;
          case 4:
            fours++;
          break;
          case 5:
            fives++;
          break;
          case 6:
            sixes++;
          break;
        }//end switch die3      
        switch ( die4 ) { //adds result of die 4 to the appropriate count 
          case 1:
            ones++;
          break;  
          case 2:
            twos++;
          break;
          case 3:
            threes++;
          break;
          case 4:
            fours++;
          break;
          case 5:
            fives++;
          break;
          case 6:
            sixes++;
          break;
        }//end switch die4     
        switch ( die5 ) { //adds result of die 5 to the appropriate count 
          case 1:
            ones++;
          break;  
          case 2:
            twos++;
          break;
          case 3:
            threes++;
          break;
          case 4:
            fours++;
          break;
          case 5:
            fives++;
          break;
          case 6:
            sixes++;
          break;
        }//end switch die5
        
        //compute upper section components 
        onesScore = ones;
        twosScore = 2*twos;
        threesScore = 3*threes;
        foursScore = 4*fours;
        fivesScore = 5*fives;
        sixesScore = 6*sixes;
        
        //compute lower section components
        //check for and compute 3 of a kind 
        //(3 of a kind score equals the sum of the 3 dice that are equivalent) 
        if ( ones==3 || twos==3 || threes==3 || fours==3 || fives==3 || sixes==3 ) { 
          if ( ones==3 ) {
            threeKind = onesScore;
          } 
          else if ( twos==3 ) {
            threeKind = twosScore;
          }
          else if ( threes==3 ) {
            threeKind = threesScore;
          }
          else if ( fours==3 ) {
            threeKind = foursScore;
          }
          else if ( fives==3 ) {
            threeKind = fivesScore;
          }
          else if ( sixes==3 ) {
            threeKind = sixesScore;
          }
        }//end if (3 of a kind)
        
        //check for and compute 4 of a kind 
        //(4 of a kind score equals the sum of the 4 dice that are equivalent) 
        if ( ones==4 || twos ==4 || threes==4 || fours ==4 || fives==4 || sixes==4 ) {
          if ( ones==4 ) {
            fourKind = onesScore;
          } 
          else if ( twos==4 ) {
            fourKind = twosScore;
          }
          else if ( threes==4 ) {
            fourKind = threesScore;
          }
          else if ( fours==4 ) {
            fourKind = foursScore;
          }
          else if ( fives==4 ) {
            fourKind = fivesScore;
          }
          else if ( sixes==4 ) {
            fourKind = sixesScore;
          }
        }//end if (4 of a kind)

        
        //check for full house
        //(full house is when there are 3 of one dice and 2 of another)
        if ( (ones==3||twos==3||threes==3||fours==3||fives==3||sixes==3) && (ones==2||twos==2||threes==2||fours==2||fives==2||sixes==2) ) {
          fullHouse = 25;
        }
        
        //check for large straight (5 consecutive numbers)
        if ( (ones==1&&twos==1&&threes==1&&fours==1&&fives==1) || (twos==1&&threes==1&&fours==1&&fives==1&&sixes==1) ) {
          lgStraight = 40;
        }
        
        //check for small straight (4 consecutive numbers)
        if ( (ones>=1&&twos>=1&&threes>=1&&fours>=1) || (twos>=1&&threes>=1&&fours>=1&&fives>=1) || (threes>=1&&fours>=1&&fives>=1&&sixes>=1) ) {
          smStraight = 30;
        }        
        
        //check for yahtzee (all 5 dice match)
        if ( ones==5 || twos==5 || threes==5 || fours==5 || fives==5 || sixes==5 ) {
          yahtzee = 50;
        }
        
        //compute chance (sum of all dice values)
        chance = onesScore+twosScore+threesScore+foursScore+fivesScore+sixesScore;
        
        //compute totals
        upperSectionSubtotal = onesScore+twosScore+threesScore+foursScore+fivesScore+sixesScore;
        if ( upperSectionSubtotal>=63 ) {//check if bonus was earned
          upperSectionBonus=35;
        }
        upperSectionTotal = upperSectionSubtotal+upperSectionBonus;
        lowerSectionTotal = threeKind+fourKind+fullHouse+smStraight+lgStraight+yahtzee+chance;
        grandTotal = upperSectionTotal+lowerSectionTotal;
        
        //print upper section initial total, upper section total including bonus, lower section total, and grand total
        System.out.println("Upper Section Initial Total = " + upperSectionSubtotal);
        System.out.println("Upper Section Total With Bonus = " + upperSectionTotal);
        System.out.println("Lower Section Total = " + lowerSectionTotal);
        System.out.println("Grand Total = " + grandTotal);
        
      }//end if (dice are valid)
      
      else {//dice are not valid
        System.out.println("Invalid Entry."); //notify user that an entry was made incorrectly
      }
            
    }//end main method
}//end Yahtzee class      