//Matthew Firestone
//CSE2 hw06
//04 March 2018
/* Gets course information from the user. 
   Data includes course number, meeting time, number of meetings, instructor, department, and number of students. 
   Checks data type of user input to ensure that numeric/string values are correct for each entry
*/ 
import java.util.Scanner;

public class Hw06 {
  
    //main method 
   	public static void main(String[] args) {
      
      Scanner scan = new Scanner(System.in); //declare scanner object
      int courseNum=0, meetingsPerWeek=0, numStudents=0;  //numeric values to be given by user
      String department="", startTime="", instructor="", junkValue=""; //string values to be given by user
      
      
      
      //get course number: user must continue to enter until they enter an integer
      
      System.out.print("Enter the course number: "); //prompt user to enter the course number
      if (scan.hasNextInt()) {//waits for user to enter something, checks if it is an integer
        courseNum=scan.nextInt(); //if int, store it as course number 
      }
      else {//if not an integer
        junkValue=scan.next();//clear scanner value
        while (true) {
          System.out.print("Invalid type. Enter a course number (integer): "); //promts user to try again
          if (scan.hasNextInt()) { //waits for user to enter because the Scanner was cleared
            courseNum=scan.nextInt(); //if int, store it as course number
            break; //ends loop when courseNum has a value
          }
          else {//if not an integer
            junkValue=scan.next(); //clear Scanner of incorrect type value
          }
        }
      }
      
      
      
      //get department: user must continue to enter until they enter a String
      
      System.out.print("Enter the department name: "); //prompt user to enter department
      if (scan.hasNextInt() || scan.hasNextDouble()) {//waits for user to enter something, checks that it's not a number
        junkValue=scan.next();//if user entered numeric value, clear Scanner
        while (true) {
          System.out.print("Invalid type. Enter a department name: "); //prompt user to try again
          if (scan.hasNextInt()||scan.hasNextDouble()) {//checks for numeric value
            junkValue=scan.next();//if user entered a number, clear Scanner
          }
          else {//if user did not enter a number, store the string as department name
            department=scan.next();
            break;//loop ends when department has a non-numeric value
          }
        }
      }
      else {//if user did not enter a number, store the string as department name
        department=scan.next();
      }
      
      
      
      //get meetings per week: user must continue to enter until they enter an integer
      
      System.out.print("Enter the number of meetings per week: "); //prompt user to enter the number
      if (scan.hasNextInt()) {//waits for user to enter something, checks if it is an integer
        meetingsPerWeek=scan.nextInt(); //if int, store it as meetings per week 
      }
      else {//if not an integer
        junkValue=scan.next();//clear scanner value
        while (true) {
          System.out.print("Invalid type. Enter a number of meetings per week (integer): "); //promts user to try again
          if (scan.hasNextInt()) { //waits for user to enter because the Scanner was cleared
            meetingsPerWeek=scan.nextInt(); //if int, store it as meetings per week
            break; //ends loop when meetingsPerWeek has a value
          }
          else {//if not an integer
            junkValue=scan.next(); //clear Scanner of incorrect type value
          }
        }
      }
      
      
      
      //get start time: user must continue to enter until they enter a String
      
      System.out.print("Enter the class start time: "); //prompt user to enter start time
      if (scan.hasNextInt() || scan.hasNextDouble()) {//waits for user to enter something, checks that it's not a number
        junkValue=scan.next();//if user entered numeric value, clear Scanner
        while (true) {
          System.out.print("Invalid type. Enter a time (for example 8:00): "); //prompt user to try again
          if (scan.hasNextInt()||scan.hasNextDouble()) {//checks for numeric value
            junkValue=scan.next();//if user entered a number, clear Scanner
          }
          else {//if user did not enter a number, store the string as start time
            startTime=scan.next();
            break;//loop ends when startTime has a non-numeric value
          }
        }
      }
      else {//if user did not enter a number, store the string as startTime name
        startTime=scan.next();
      }
      
      
      
      //get instructor: user must continue to enter until they enter a String
      
      System.out.print("Enter the instructor's name: "); //prompt user to enter instructor
      if (scan.hasNextInt() || scan.hasNextDouble()) {//waits for user to enter something, checks that it's not a number
        junkValue=scan.next();//if user entered numeric value, clear Scanner
        while (true) {
          System.out.print("Invalid type. Enter an instructor name: "); //prompt user to try again
          if (scan.hasNextInt()||scan.hasNextDouble()) {//checks for numeric value
            junkValue=scan.next();//if user entered a number, clear Scanner
          }
          else {//if user did not enter a number, store the string as instructor name
            instructor=scan.next();
            break;//loop ends when instructor has a non-numeric value
          }
        }
      }
      else {//if user did not enter a number, store the string as instructor name
        instructor=scan.next();
      }
      
      
      
      //get number of students: user must continue to enter until they enter an integer
      
      System.out.print("Enter the number of students: "); //prompt user to enter the number of students in the course
      if (scan.hasNextInt()) {//waits for user to enter something, checks if it is an integer
        numStudents=scan.nextInt(); //if int, store it as numStudents 
      }
      else {//if not an integer
        junkValue=scan.next();//clear scanner value
        while (true) {
          System.out.print("Invalid type. Enter a number of students (integer): "); //promts user to try again
          if (scan.hasNextInt()) { //waits for user to enter because the Scanner was cleared
            numStudents=scan.nextInt(); //if int, store it as numStudents
            break; //ends loop when numStudents has a value
          }
          else {//if not an integer
            junkValue=scan.next(); //clear Scanner of incorrect type value
          }
        }
      }
      
      
      
      //PRINT all course information to the screen:
      System.out.println("Course Number: " + courseNum);
      System.out.println("Department: " + department);
      System.out.println("Meetings per week: " + meetingsPerWeek);
      System.out.println("Start Time: " + startTime);
      System.out.println("Instructor Name: " + instructor);
      System.out.println("Number of Students: " + numStudents);
      
    }//end main
  
}//end class
      