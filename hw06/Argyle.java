//Matthew Firestone
//CSE2 hw06
//21 March 2018
/* Argyle program: 
   Prints argyle design based on user entered parameters
*/ 
import java.util.Scanner;

public class Argyle {
  
  // main method 
  public static void main(String[] args) {
    
    Scanner scan = new Scanner (System.in); //create scanner object
    String junkValue="";//used to clear scanner of incorrect type values
    int width=0, height=0, diamond=0, stripe=0; //values entered by user for argyle pattern
    
    
    //GET DATA FROM USER AND VALIDATE NUMBERS:
    
    //get width of window from user    
    while (width<=0) {
      System.out.print("Enter the width of the viewing window in characters: ");
      
      if (scan.hasNextInt()) {//waits for user to enter something, checks if it is an integer
       width=scan.nextInt(); //if int, store it as width
      }
      else {//if not an integer
        junkValue=scan.next();//clear scanner value
        while (true) {
          System.out.print("Invalid. Enter a positive integer: "); //promts user to try again
          if (scan.hasNextInt()) { //waits for user to enter because the Scanner was cleared
            width=scan.nextInt(); //if int, store it as width
            break; //ends loop when width has a value
          }
          else {//if not an integer
            junkValue=scan.next(); //clear Scanner of incorrect type value
          }
        }
      }     
      if (width<=0) {
        System.out.print("Invalid. Must be greater than zero. ");
      }
      
    }//ends when width is positive

    
    
    //get height of window from user
    while (height<=0) {
      System.out.print("Enter the height of the viewing window in characters: ");
      
      if (scan.hasNextInt()) {//waits for user to enter something, checks if it is an integer
       height=scan.nextInt(); //if int, store it as height
      }
      else {//if not an integer
        junkValue=scan.next();//clear scanner value
        while (true) {
          System.out.print("Invalid. Enter a positive integer: "); //promts user to try again
          if (scan.hasNextInt()) { //waits for user to enter because the Scanner was cleared
            height=scan.nextInt(); //if int, store it as height
            break; //ends loop when height has a value
          }
          else {//if not an integer
            junkValue=scan.next(); //clear Scanner of incorrect type value
          }
        }
      }     
      if (height<=0) {
        System.out.print("Invalid. Must be greater than zero. ");
      }
      
    }//ends when height is positive

    
    
    //get diamond width from user
    while (diamond<=0) {
      System.out.print("Enter the width of the diamonds in characters: ");
      
      if (scan.hasNextInt()) {//waits for user to enter something, checks if it is an integer
       diamond=scan.nextInt(); //if int, store it as diamond
      }
      else {//if not an integer
        junkValue=scan.next();//clear scanner value
        while (true) {
          System.out.print("Invalid. Enter a positive integer: "); //promts user to try again
          if (scan.hasNextInt()) { //waits for user to enter because the Scanner was cleared
            diamond=scan.nextInt(); //if int, store it as diamond
            break; //ends loop when diamond has a value
          }
          else {//if not an integer
            junkValue=scan.next(); //clear Scanner of incorrect type value
          }
        }
      }     
      if (diamond<=0) {
        System.out.print("Invalid. Must be greater than zero. ");
      }
      
    }//ends when diamond is positive

    
    //get stripe width from user
    while (stripe<=0 || stripe%2==0 || stripe>diamond/2) {
      System.out.print("Enter the width of the argyle stripe in characters: ");
      
      if (scan.hasNextInt()) {//waits for user to enter something, checks if it is an integer
       stripe=scan.nextInt(); //if int, store it as stripe
      }
      else {//if not an integer
        junkValue=scan.next();//clear scanner value
        while (true) {
          System.out.print("Invalid. Enter a positive integer: "); //promts user to try again
          if (scan.hasNextInt()) { //waits for user to enter because the Scanner was cleared
            stripe=scan.nextInt(); //if int, store it as stripe
            break; //ends loop when stripe has a value
          }
          else {//if not an integer
            junkValue=scan.next(); //clear Scanner of incorrect type value
          }
        }
      }     
      if (stripe<=0 || stripe%2==0 || stripe>diamond/2) {
        System.out.println("Invalid. Must be positive, odd-numbered, and no more than " + diamond/2);
      }
      
    }//ends when stripe is positive, odd, and no more than half of diamond
    
    
    
    //Get the three characters to fill pattern (from user)
    System.out.print("Enter a character for the square pattern fill: ");
    String temp = scan.next();
		char s = temp.charAt(0); //s will represent the square pattern character
    
    System.out.print("Enter a character for the diamond pattern fill: ");
    temp = scan.next();
		char d = temp.charAt(0); //d will represent the diamond pattern character
    
    System.out.print("Enter a character to make up the X pattern(stripes): ");
    temp = scan.next();
		char x = temp.charAt(0); //x will represent the x pattern, or striped pattern, character
    

    /*PRINT ARGYLE PATTERN:
      Four differnet patterns exist, representing the four quarters of each X pattern (Also each full diamond)
      Each section has different patterns for determining which character should be placed where
      (Top left, top right, bottom left, bottom right)
    */
   
    for (int i=1; i<=height; i++) {//prints rows until reaching the height limit set by user
      
      for (int j=1; j<=width; j++) {//prints characters in a row until reaching width limit set by user
      
        if ( ((i-1)/diamond)%2==0 ) {//checks if we are in the top half of the pattern
        
          if ( ((j-1)/diamond)%2==0 ) {//if left top portion of pattern

            if ( Math.abs((i-1)%diamond-(j-1)%diamond) <= (stripe-1)/2 ) {
              System.out.print(x);
            }
            else {
              if ( (j-1)%diamond + (i-1)%diamond >= diamond ) {
                System.out.print(d);
              }
              else {
                System.out.print(s);
              }
            }
          }

          else {//right top portion of pattern

            if ( Math.abs(((i-1)%diamond+(j-1)%diamond) - (diamond-1)) <= (stripe-1)/2 ) {
              System.out.print(x);
            }
            else {
              if ((j-1)%diamond<(i-1)%diamond) {
                System.out.print(d);    
              }
              else {
                System.out.print(s);
              }
            }

          } 
        }
       
        else {//bottom half of the pattern     
          
         if ( ((j-1)/diamond)%2==0 ) {//bottom left portion
           
           if ( Math.abs(((i-1)%diamond+(j-1)%diamond) - (diamond-1)) <= (stripe-1)/2 ) {
            System.out.print(x);
           }
           else {
            if ( (j-1)%diamond >= (i-1)%diamond ) {
             System.out.print(d);    
            }
            else {
              System.out.print(s);
            }
           }
           
         }   
          
         else {//bottom right portion
           if ( Math.abs((i-1)%diamond-(j-1)%diamond) <= (stripe-1)/2 ) {
             System.out.print(x);
           }
           else {
             if ( (j-1)%diamond + (i-1)%diamond < diamond ) {
               System.out.print(d);
             }
             else {
               System.out.print(s);
             }
           }           
         }
       }
       
      
      }//end of a row 
      System.out.println();//go to next row
      
    }//end of last row
    
    
    
  }//end main
  
}//end class Argyle
      