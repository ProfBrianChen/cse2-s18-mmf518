//Matthew Firestone
//CSE2 lab09
//13 April 2018
/*  
   
*/ 

public class Lab09 {
  
  // main method 
  public static void main ( String[] args ) {
    int[] array0 = new int[]{1,2,3,4,5,6,7,8};
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    
    inverter(array0);
    print(array0);
    
    inverter2(array1);
    print(array1);
    
    int[] array3 = inverter2(array2);
    print(array3);
    
  }
  
  public static int[] copy ( int[] input ) {
    int[] output = new int[input.length];
    for ( int i=0; i<output.length; i++ ) {
      output[i] = input[i];
    }
    return output;
  }
  
  public static void inverter ( int[] input ) {
    for ( int i=0; i<input.length/2; i++ ) {
      int temp = input[i];
      input[i] = input[input.length-1-i];
      input[input.length-1-i] = temp;
    }
  }
  
  public static int[] inverter2 ( int[] input ) {
    int[] output = copy(input);
    for ( int i=0; i<output.length/2; i++ ) {
      int temp = output[i];
      output[i] = output[output.length-1-i];
      output[output.length-1-i] = temp;
    }
    return output;
  }
  
  public static void print ( int[] input ) { 
    for ( int i=0; i<input.length; i++ ) {
      System.out.print(input[i] + " ");
    }
    System.out.println();
  }
  
  
}