//Matthew Firestone
//CSE2 hw03 Program #1 of 2
//10 Feb 2018
/* Convert program: 
   Prompts the user for number of acres of land affected by hurricane precipitation and inches of rain. 
   Returns the amount of rain in cubic miles.  */

import java.util.Scanner;

public class Convert {
  
    //main method 
   	public static void main(String[] args) {
      
      Scanner myScanner = new Scanner( System.in ); //declare scanner object
      final double conversionFactor = 0.000000024654832; //number of cubic miles per acre-inch(acre times inch) 
                                                         //source: https://water.usgs.gov/edu/earthrain.html
      
      System.out.print("Enter the affected area in acres: "); //prompt user to enter acres of land affected
      double landArea = myScanner.nextDouble(); //read user input of acres of affected land area 
      
      System.out.print("Enter the inches of rainfall in the affected area: "); //prompt user to enter rainfall in inches
      double inchesOfRain = myScanner.nextDouble(); //read user input of inches of rain 
      
      double cubicMiles = landArea*inchesOfRain*conversionFactor; //calculate volume in acres*inches and convert to cubic miles
      
      System.out.println("Amount of rain: " + cubicMiles + " cubic miles."); //print converted volume 
      
    }//end main
  
}//end Convert class