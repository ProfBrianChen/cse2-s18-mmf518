//Matthew Firestone
//CSE2 hw03 Program #2 of 2
//10 Feb 2018
/* Pyramid program: 
   Prompts the user for dimensions of a pyramid
   Returns the volume.  */

import java.util.Scanner;

public class Pyramid {
  
    //main method 
   	public static void main(String[] args) {
      
      Scanner myScanner = new Scanner( System.in ); //declare scanner object
      
      System.out.print("The square side of the pyramid is (input length): "); //prompt user to enter square side length
      double sideLength = myScanner.nextDouble(); //read user input of length of pyramid side 
      
      System.out.print("The height of the pyramid is (input height): "); //prompt user to enter height
      double height = myScanner.nextDouble(); //read user input of pyramid height
      
      double volume = (sideLength*sideLength*height)/3; //calculate volume
      
      System.out.println("The volume inside the pyramid is: " + volume); //prints result for pyramid volume

      
    }//end main
  
}//end Pyramid class