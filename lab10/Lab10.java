//Matthew Firestone
//CSE2 Lab 10

public class Lab10 {
  
  public static void main ( String[] args ) {
    
    
    
  }
  
  public static int[][] increasingMatrix ( int height, int width, boolean format ) {
    int array[][];
    if ( format ) {
      array = new int[height][width];
      for ( int r=0; r<height; r++ ) {
        for ( int c=0; c<width; c++ ) {
          array[r][c] = c + r*width;
        }
      }
    }
    else {
      array = new int[width][height];
      for ( int c=0; c<width; c++ ) {
        for ( int r=0; r<height; r++ ) {
          array[c][r] = c + r*width;
        }
      }
    }
    return array;
  }
  
  public static void printMatrix ( int[][] array, boolean format ) {
    if ( format ) {
      int height = array.length;
      int width = array[0].length;
      for ( int r=0; r<height; r++ ) {
        for ( int c=0; c<width; c++ )
        {
          System.out.print(array[r][c] + " ");
        }
        System.out.println();
      }
    }
    else {
      int height = array[0].length;
      int width = array.length;
      for ( int r=0; r<height; r++ ) {
        for ( int c=0; c<width; c++ )
        {
          System.out.print(array[c][r] + " ");
        }
        System.out.println();
      }
    }
  }
  
}