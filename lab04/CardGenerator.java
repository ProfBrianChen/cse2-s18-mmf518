//Matthew Firestone
//CSE2 lab04
//16 Feb 2018
/* CardGenerator program: 
   Generates a number from 1-52 to represent a card in a deck
   Prints the suit and name of the card
*/ 
public class CardGenerator {
  
    // main method 
   	public static void main(String[] args) {
      
      int cardNumber = ((int)(Math.random()*52)) + 1; //generate random number from 1-52
      String suit = ""; //stores name of Suit 
      String cardIdentity = ""; //stores name of card, such as King or Five
      
      //if statements to determine suit
      if ( cardNumber>=1 && cardNumber<=13 ) {//the numbers 1-13 represent Diamonds
        suit = "Diamonds";
      }
      if ( cardNumber>=14 && cardNumber<=26 ) {//the numbers 14-26 represent Clubs
        suit = "Clubs";
      }  
      if ( cardNumber>=27 && cardNumber<=39 ) {//the numbers 27-39 represent Hearts
        suit = "Hearts";
      }      
      if ( cardNumber>=40 && cardNumber<=52 ) {//the numbers 40-52 represent Spades
        suit = "Spades";
      }      
      
      //determine value of card by it's place relative to the King
      switch ( cardNumber%13 ) {
        case 0: cardIdentity = "King";
          break;
        case 1: cardIdentity = "Ace";
          break;
        case 2: cardIdentity = "Two";
          break;
        case 3: cardIdentity = "Three";
          break;
        case 4: cardIdentity = "Four";
          break;
        case 5: cardIdentity = "Five";
          break;        
        case 6: cardIdentity = "Six";
          break;
        case 7: cardIdentity = "Seven";
          break;
        case 8: cardIdentity = "Eight";
          break;
        case 9: cardIdentity = "Nine";
          break;
        case 10: cardIdentity = "Ten";
          break;
        case 11: cardIdentity = "Jack";
          break;           
        case 12: cardIdentity = "Queen";
          break;           
      }//end switch
      
      //System.out.println(cardNumber);
      System.out.println("You picked the " +cardIdentity+ " of " +suit);
      
    }//end main method
  
}//end CardGenerator class
