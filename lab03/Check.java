//Matthew Firestone
//CSE2 lab03
//09 Feb 2018
/* Check program: Used to split a restaurant check among a group of people.
   Uses the Scanner class to input cost of the check, percentage tip they wish to pay, and the number of people
   Prints how much each person needs to pay
*/ 
import java.util.Scanner;

public class Check {
  
    	// main method 
   	public static void main(String[] args) {
      
      Scanner myScanner = new Scanner( System.in ); //declare scanner object
      
      System.out.print("Enter the original cost of the check (in the form xx.xx): "); //prompt user to enter bill amount
      double checkCost = myScanner.nextDouble(); //read user input of bill amount and store as double
      
      System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //prompt user for tip percentage
      double tipPercent = myScanner.nextDouble(); //read user input of Tip Percentage and store as double
      tipPercent /= 100;   //Convert the percentage into a decimal value
      
      System.out.print("Enter the number of people who went out to dinner: "); //prompt user for number of people
      int numPeople = myScanner.nextInt(); //read user input of number of people and store as integer

      double totalCost;  //total check amount
      double costPerPerson; //to be paid per person
      int dollars,       //whole dollar amount of cost (such as 10 for $10.25)
        dimes, pennies;  //for storing digits to the right of the decimal point for the costs
      
      totalCost = checkCost * (1 + tipPercent); //adds tip to total given by user
      costPerPerson = totalCost / numPeople;    //gets cost per person as double
      dollars=(int)costPerPerson;               //gets dollar amount (first digit)
      dimes=(int)(costPerPerson * 10) % 10;     //gets second digits (tenths place)
      pennies=(int)(costPerPerson * 100) % 10;  //gets number of pennies (hundredths)
      
      System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);

      
    }//end main
  
}//end Check class
