//Matthew Firestone
//CSE2 hw07
//27 March 2018
/* Area program: 
   Takes user input on shape and dimensions of shape
   Prints area of the user's shape
*/ 
import java.util.Scanner;

public class Area {
  
  //main method 
  public static void main(String[] args) {
    
    Scanner scan = new Scanner (System.in); //create scanner object
    String shape = "";
    double area=0;

    shape = getShape();//get shape entered from user
    //check for valid shape, and if not, re-enter:
    while ( !shape.equals("circle") && !shape.equals("triangle") && !shape.equals("rectangle") ) {
      System.out.println("Invalid shape entry. Try again.");
      shape = getShape();
    }
    
    switch (shape) {//based on chosen shape, determines the correct area method to use
      case "triangle":
        area = triangle();
        break;
      case "rectangle":
        area = rectangle();
        break;
      case "circle":
        area = circle();
        break;
    }
    
    System.out.println("The area is " + area);
    
  }//end main  
  
  
  
  public static String getShape() {
    Scanner scan = new Scanner (System.in); //create scanner object
    System.out.print("Please enter 'rectangle', 'triangle', or 'circle': ");
    String shape = scan.next();
    return shape;
  }
  
  public static double triangle() {
    Scanner scan = new Scanner (System.in); //create scanner object
    String junkValue="";
    double height=0, base=0, area=0;
    System.out.print("Enter the height of the triangle: ");
    
    //get double for height:
    if (scan.hasNextDouble()) {//waits for user to enter something, checks if it is a double
        height=scan.nextDouble(); //if double, store it as height 
      }
    else {//if not double
      junkValue=scan.next();//clear scanner value
      while (true) {
        System.out.print("Invalid type. Enter a double: "); //promts user to try again
        if (scan.hasNextDouble()) { //waits for user to enter because the Scanner was cleared
          height=scan.nextDouble(); //if double, store it as height
          break; //ends loop when height has a value
        }
        else {//if not double
          junkValue=scan.next(); //clear Scanner of incorrect type value
        }
      }
    }
    
    System.out.print("Enter the base of the triangle: ");
    //get double for base:
    if (scan.hasNextDouble()) {//waits for user to enter something, checks if it is a double
        base=scan.nextDouble(); //if double, store it as base 
      }
    else {//if not double
      junkValue=scan.next();//clear scanner value
      while (true) {
        System.out.print("Invalid type. Enter a double: "); //promts user to try again
        if (scan.hasNextDouble()) { //waits for user to enter because the Scanner was cleared
          base=scan.nextDouble(); //if double, store it as base
          break; //ends loop when base has a value
        }
        else {//if not double
          junkValue=scan.next(); //clear Scanner of incorrect type value
        }
      }
    }
    
    area = base*height*0.5;
    return area;
  }//end triangle method
  
  public static double circle() {
    Scanner scan = new Scanner (System.in); //create scanner object
    String junkValue="";
    double radius=0, area=0;
    System.out.print("Enter the radius of the circle: ");
    
    //get double for radius:
    if (scan.hasNextDouble()) {//waits for user to enter something, checks if it is a double
        radius=scan.nextDouble(); //if double, store it as radius 
      }
    else {//if not double
      junkValue=scan.next();//clear scanner value
      while (true) {
        System.out.print("Invalid type. Enter a double: "); //promts user to try again
        if (scan.hasNextDouble()) { //waits for user to enter because the Scanner was cleared
          radius=scan.nextDouble(); //if double, store it as radius
          break; //ends loop when radius has a value
        }
        else {//if not double
          junkValue=scan.next(); //clear Scanner of incorrect type value
        }
      }
    }
    
    area = radius*radius*3.14;
    return area;
  }//end circle method
  
  public static double rectangle() {
    Scanner scan = new Scanner (System.in); //create scanner object
    String junkValue="";
    double height=0, width=0, area=0;
    System.out.print("Enter the height of the rectangle: ");
    
    //get double for height:
    if (scan.hasNextDouble()) {//waits for user to enter something, checks if it is a double
        height=scan.nextDouble(); //if double, store it as height 
      }
    else {//if not double
      junkValue=scan.next();//clear scanner value
      while (true) {
        System.out.print("Invalid type. Enter a double: "); //promts user to try again
        if (scan.hasNextDouble()) { //waits for user to enter because the Scanner was cleared
          height=scan.nextDouble(); //if double, store it as height
          break; //ends loop when height has a value
        }
        else {//if not double
          junkValue=scan.next(); //clear Scanner of incorrect type value
        }
      }
    }
    
    System.out.print("Enter the width of the rectangle: ");
    //get double for width:
    if (scan.hasNextDouble()) {//waits for user to enter something, checks if it is a double
        width=scan.nextDouble(); //if double, store it as width 
    }
    else {//if not double
      junkValue=scan.next();//clear scanner value
      while (true) {
        System.out.print("Invalid type. Enter a double: "); //promts user to try again
        if (scan.hasNextDouble()) { //waits for user to enter because the Scanner was cleared
          width=scan.nextDouble(); //if double, store it as width
          break; //ends loop when width has a value
        }
        else {//if not double
          junkValue=scan.next(); //clear Scanner of incorrect type value
        }
      }
    }
    
    area = width*height;
    return area;
  }//end rectangle method
  
}//end Area class  