//Matthew Firestone
//CSE2 hw07
//27 March 2018
/* String Analysis program: 
   Analyzes each character (or a specified number of characters) 
   of a string to determine if each char is a letter (a-z lowercase)
*/ 

import java.util.Scanner;

public class StringAnalysis {
  
  //main method 
  public static void main(String[] args) {
    
    boolean chooseNumber=false; //this is whether the user will specify a number of chars
    boolean allLetters=false; //final result - are all chars letters?
    String userString=""; //the analyzed string, entered by user 
    
    Scanner scan = new Scanner (System.in); //create scanner object
    
    System.out.print("If you want to choose only a certain number of characters to analyze, type 'yes': ");
    String userChoice = scan.next();
    if (userChoice.equals("yes")) { //if user said yes, they want to enter a number of chars
      chooseNumber=true;
    }
    
    System.out.print("Enter a string to be analyzed: "); //prompt user for their String and store it 
    userString = scan.next();
    
    
    if (chooseNumber) {//get optional number of chars from user 
      System.out.print("How many characters do you want to analyze? ");
      int numChars = scan.nextInt();
      allLetters = analysis(userString, numChars); //pass number to method if there is one 
    }
    else {
      allLetters = analysis(userString); //version of method with no number 
    }
    
    //display result:
    if (allLetters) {
      System.out.println("All characters are letters.");
    }
    else {
      System.out.println("All characters are not letters.");
    }
    
  }//end main
  
  public static boolean analysis(String input) {
    
    int letterCounter=0;
    for (int i=0; i<input.length(); i++)
    {
      if ( input.charAt(i) >= 'a' && input.charAt(i) <= 'z' ) {
        letterCounter++;
      }
    }
    
    if ( letterCounter==input.length() ) {
      return true;
    }
    else {
      return false;
    }
    
  }
  
  public static boolean analysis(String input, int numChars) {
    
    if ( numChars > input.length() ) {
      numChars = input.length();
    }
    
    int letterCounter=0;
    for (int i=0; i<numChars; i++)
    {
      if ( input.charAt(i) >= 'a' && input.charAt(i) <= 'z' ) {
        letterCounter++;
      }
    }
    
    if ( letterCounter==numChars ) {
      return true;
    }
    else {
      return false;
    }
    
    
  }
  
}//end class