//Matthew Firestone
//CSE2 hw08
//8 April 2018
/*  
   
*/ 
import java.util.Random;
import java.util.Scanner;

public class CSE2Linear {
  
  //main method 
  public static void main(String[] args) {
    
    Scanner scan = new Scanner(System.in);  //declare scanner object
    int[] grades = new int[15]; //integer array to hold grades of 15 students
    boolean validGrade=false; //used to test validity of user entry grades
    String junkValue = ""; //stores incorrect type values entered by user 
    
    for (int i=0; i<grades.length; i++) { //user will enter a value for each grade in the array
      
      while ( !validGrade ) { //continue getting input until it is acceptable
        System.out.print("Enter the grade for student " + (i+1) + ": "); //prompt user
        if ( scan.hasNextInt() ) {   //check for integer
          grades[i] = scan.nextInt();   //store value if int 
          if ( grades[i]>=0 && grades[i]<=100 ) {   //check if in range of 0-100
            if ( i!=0 ) {   //only need to evalute previous grade if this is not the first 
              if ( grades[i] >= grades[i-1] ) {   //check if ascending
                validGrade=true;  //all conditions are satisfied
              }
              else {
                //error 3
                System.out.println("Error: Grade cannot be lower than previous grade.");
              }
            }
            else {
              validGrade=true;  //3rd condition is not necessary for the first grade entered
            }
          }
          else {
            //error 2
            System.out.println("Error: Grade must be between 0 and 100.");
          }
        }
        else {
          //error 1
          junkValue = scan.next(); //clears non-int from the scanner
          System.out.println("Error: Not an integer.");
        }
      } //ends when there is a valid grade entered
      validGrade=false;
      
    } //end of user entries
    
    //print grades
    System.out.print("Student Grades:");
    for ( int i=0; i<grades.length; i++) {
      System.out.print(" " + grades[i]);
    }
    System.out.println();
    
    //binary search
    System.out.println("Binary Search:");
    binarySearch(grades);
    
    //scramble
    scramble(grades);
    
    //linear search
    System.out.println("Linear Search:");
    System.out.print("Scrambled Student Grades:");
    for ( int i=0; i<grades.length; i++) {
      System.out.print(" " + grades[i]);
    }
    System.out.println();
    linearSearch(grades);
    

  } //end main

  public static void scramble (int[] grades) {
    Random rand = new Random();
    int temp=0, target=0;
    for ( int i=0; i<grades.length; i++ ) {
      target = rand.nextInt(grades.length);
      temp = grades[target];
      grades[target] = grades[i];
      grades[i] = temp;
    }
  }
  
  public static void binarySearch(int[] grades) {
    Scanner scan = new Scanner(System.in);
    int low = 0, high = grades.length-1, middle = high/2;
    int searchVal = 0, iterations = 0;
    String junkValue = "";
    boolean validSearch = false, found = false;
    
    while ( !validSearch ) {
      System.out.print("Enter a grade to search for: ");
      if ( scan.hasNextInt() ) {
        searchVal = scan.nextInt();
        validSearch=true;
      }
      else {
        System.out.println("Error: Not an integer.");
        junkValue = scan.next();
      }
    }
    
    while ( low<=high && !found ) {
      if ( grades[middle]==searchVal ) {
        found=true;
      }
      if ( grades[middle] > searchVal ) {
        high=middle-1;
        middle=(high+low)/2;
      }
      if ( grades[middle] < searchVal ) {
        low=middle+1;
        middle=(high+low)/2;
      }
      iterations++;
    }
    
    if (found) {
      System.out.println(searchVal + " was found after " + iterations + " iteration(s).");
    }
    else {
      System.out.println(searchVal + " was not found after " + iterations + " iterations.");
    }
  } //end binary search
  
  public static void linearSearch(int[] grades) {
    Scanner scan = new Scanner(System.in);
    String junkValue = "";
    int searchVal=0, iterations=0;
    boolean found = false, validSearch = false;
    
    while ( !validSearch ) {
      System.out.print("Enter a grade to search for: ");
      if ( scan.hasNextInt() ) {
        searchVal = scan.nextInt();
        validSearch=true;
      }
      else {
        System.out.println("Error: Not an integer.");
        junkValue = scan.next();
      }
    }
    
    for (int i=0; i<grades.length; i++) {
      iterations++;
      if ( grades[i] == searchVal ) {
        found=true;
        break;
      }
    }
    
    if (found) {
      System.out.println(searchVal + " was found after " + iterations + " iteration(s).");
    }
    else {
      System.out.println(searchVal + " was not found after " + iterations + " iterations.");
    }
   
  }//end linear search
  
}//end class