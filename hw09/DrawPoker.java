//Matthew Firestone
//CSE2 hw09
//8 April 2018
   
import java.util.Random;

public class DrawPoker {
  
  //main method 
  public static void main(String[] args) {
    
    int[] deck = new int[52]; //declare deck array
    for ( int i=0; i<deck.length; i++ ) {    //assign values 0-51 to ordered deck
      deck[i] = i;
    }
    
    shuffle(deck); 
    
    //Declare arrays for each player's hand:
    int[] hand1 = new int[5]; 
    int[] hand2 = new int[5];
    
    //Deals 2 hands (5 cards each) by alternating the top 10 cards in the shuffled deck
    for ( int i=0; i<hand1.length; i++ ) { 
      hand1[i] = deck[i*2];
      hand2[i] = deck[i*2+1];
    }
    
    String winner = "";
    
    //check for win by flush 
    if ( flush(hand1) && !flush(hand2) ) {
      winner = "Hand 1 wins.";
    }
    
    if ( flush(hand2) && !flush(hand1) ) {
      winner = "Hand 2 wins.";
    }
    
    if ( flush(hand1) && flush(hand2) ) {
      winner = "It's a tie.";
    }
    
    //check for win by pair
    if ( pair(hand1) && !pair(hand2) ) {
      winner = "Hand 1 wins.";
    }
    
    if ( pair(hand2) && !pair(hand1) ) {
      winner = "Hand 2 wins.";
    }
    
    if ( pair(hand1) && pair(hand2) ) {
      winner = "It's a tie.";
    }
    
    //check for neither hand getting pair or flush
    if ( !pair(hand1) && !pair(hand2) && !flush(hand1) && !flush(hand2) ) {
      winner = "It's a tie.";
    }
    
    
    System.out.println(winner);
    
  }
  
  public static void shuffle ( int[] deck ) {
    Random rand = new Random();
    int temp=0, target=0;
    for ( int i=0; i<deck.length; i++ ) {
      target = rand.nextInt(deck.length);
      temp = deck[target];
      deck[target] = deck[i];
      deck[i] = temp;
    }
  }
  
  public static boolean flush ( int[] hand ) {
    //cards 1-4 must match the suit of card 0
    int matches =0;
    for ( int i=1; i<5; i++ ) { //checks each card to see if it matches the first
      if ( hand[i]/13 == hand[0]/13 ) { //this will give a range of 0-3 representing suits.
        matches++;
      }
    }
    
    if ( matches == 4) {//if the 4 other cards had same suit as the first
      return true;
    }
    else {
      return false;
    }
  }
  
  public static boolean pair ( int[] hand ) {
    //determine if any card matches any other card
    boolean match=false;
    //test each possible pair:
    for ( int i=0; i<5; i++ ) {
      for ( int j=0; j<5; j++ ) {
        if ( j!=i && hand[i]%13 == hand[j]%13 ) {
          match=true;
        }
      }
    }
    return match;
  }
 
  
}
