Grade:  90/100
Comments: 
A) Does the code compile?  How can any compiler errors be resolved?
Code compiles correctly.
B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
Code runs without RE, however your only output is the result of the game, not the hands of the players so it is very tedious to determine if your program actually works correctly.
C) How can any runtime errors be resolved?
N/A
D) What topics should the student study in order to avoid the errors they made in this homework?
N/A
E) Other comments:
