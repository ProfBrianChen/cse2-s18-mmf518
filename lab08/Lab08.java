//Matthew Firestone
//CSE2 lab08
//6 April 2018
/* Array program 
   
*/ 
import java.util.Random;
import java.util.Scanner;

public class Lab08 {
  
  // main method 
  public static void main(String[] args) {
    
    Random rand = new Random();
    Scanner scan = new Scanner(System.in);
    String[] students = new String[rand.nextInt(6)+5];
    int[] midterm = new int[students.length];
    for (int i=0; i<students.length; i++) {
      System.out.print("Enter a student name: ");
      students[i] = scan.next();
      midterm[i] = rand.nextInt(101);
    }
    for (int i=0; i<students.length; i++) {
      System.out.println(students[i] + ": " + midterm[i]);
    }
    
  }//end main
}
