//Matthew Firestone
//CSE2 hw10
/*Robot City program:
  A city with a 10-15 by 10-15 grid of blocks is generated
  Each block has 100-999 in population
  A random number of robots (1-10) invades
  5 updates are shown as the robots move each time
  A negative value represents a robot 
*/
import java.util.Random;

public class RobotCity {
  
  //main method
  public static void main ( String[] args ) {
    Random rand = new Random();
    int[][] city = buildCity();
    System.out.println("City:");
    display(city);
    invade(city, rand.nextInt(10)+1);
    System.out.println("Invade:");
    display(city);
    for (int i=1; i<=5; i++) { //perform 5 robot moves
      update(city);
      System.out.println("Update " + i + ":");
      display(city);
    }
  } //end main
  
  //Create city matrix with random size and population
  public static int[][] buildCity (  ) {
    Random rand = new Random();
    int eastWestDimension = rand.nextInt(6)+10;
    int northSouthDimension = rand.nextInt(6)+10;
    int[][] city = new int[eastWestDimension][northSouthDimension];
    for ( int i=0; i<eastWestDimension; i++ ) {
      for ( int j=0; j<northSouthDimension; j++ ) {
        city[i][j] = rand.nextInt(900)+100;
      }
    }
    return city;
  }
  
  //Print out the current city matrix
  public static void display ( int[][] city ) {
    for ( int i=0; i<city.length; i++ ) {
      for ( int j=0; j<city[0].length; j++ ) {
        if ( city[i][j] >= 0 ) {
          System.out.print(" ");
        }
        System.out.print(city[i][j] + "  ");
      }
      System.out.println();
    }
    System.out.println();
  }
  
  //puts the specified number of robots onto their own unique random block 
  public static void invade ( int[][] city, int k ) {
    int i=0, j=0;
    for ( int r=0; r<k; r++ ) {
      do {
        i = rand.nextInt(city.length);
        j = rand.nextInt(city[0].length);
      } while ( city[i][j] < 0 );
      city[i][j] *= -1;
    }
  }
  
  //moves all robots one position to the left in the city matrix
  public static void update ( int[][] city ) {
    for ( int i=0; i<city.length; i++ ) {
      for ( int j=0; j<city[0].length; j++ ) {
        if ( city[i][j] < 0 ) {
          city[i][j] *= -1;
          if ( j != 0 ) {
            city[i][j-1] *= -1;
          }
        } 
      }
    }    
  } 
  
}