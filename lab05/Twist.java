//Matthew Firestone
//CSE2 lab05
//2 March 2018
/* Twist program: 
   prints twist pattern with the number of characters of length given by the user 
   checks to ensure a positive integer was entered
*/ 
import java.util.Scanner;

public class Twist {
  
  // main method 
  public static void main(String[] args) {
    
    Scanner scan = new Scanner (System.in); //create scanner object
    int length=-1; //number of chars in twist, must become a positive integer by user entry 
    boolean correctValue=false; //when true, the user has entered a positive integer
    String junkWord = ""; //used to dump incorrect values given by user
    
    while (correctValue!=true) { //loop stops when a positive integer for length is stored
          System.out.print("Enter a positive integer: "); //prompt user for length
          if (scan.hasNextInt()) { //check that integer was entered
            length = scan.nextInt(); //if user integer, store it as length
            if (length>=0) { //check that user's integer was not negative
              correctValue = true; 
            } 
            else {
              System.out.println("Not a positive number."); //error message if user did not enter positive number
            }
          }
          else {
            junkWord = scan.next(); //used to dump incorrect value given by user so they can try again
          }
    }
        
    
    //print message
    
    int lineLength=0; //index value from left to right on the current line
    char next = ' '; //stores the char that will be printed next in line 
    
    //print line1
    while (lineLength<length) {
        switch (lineLength%3) {
          case 0: 
            next = '\\';
            break;
          case 1:
            next = ' ';
            break;
          case 2:
            next = '/';
            break;
        }
        System.out.print(next); //adds a character to the left of the line
        lineLength++; 
    }
    System.out.println(); //skip to line 2
    lineLength = 0; //reset length for beginning new line
    
    
    //print line 2
    while (lineLength<length) {
        switch (lineLength%3) {
          case 0: 
            next = ' ';
            break;
          case 1:
            next = 'X';
            break;
          case 2:
            next = ' ';
            break;
        }
        System.out.print(next);
        lineLength++;
    }
    System.out.println(); //move to line 3
    lineLength = 0; //reset length for beginning new line

    
    //print line 3
    while (lineLength<length) {
        switch (lineLength%3) {
          case 0: 
            next = '/';
            break;
          case 1:
            next = ' ';
            break;
          case 2:
            next = '\\';
            break;
        }
        System.out.print(next);
        lineLength++;
    }
    System.out.println();
    //after line 3, done printing
    
  }//end main
  
}//end method Twist
      