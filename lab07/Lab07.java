//Matthew Firestone
//CSE2 lab07
//6 April 2018
/* Random sentence program 
   
*/ 
import java.util.Random;
import java.util.Scanner;

public class Lab07 {
  
  // main method 
  public static void main(String[] args) {
    
    paragraph();
    
  }//end main
  
  
  public static String adjective() {
    Random rand = new Random();
    int i = rand.nextInt(10);
    String s="";
    switch (i) {
      case 0:
        s = "sharp";
        break;
      case 1:
        s = "invincible";
        break;
      case 2:
        s = "skillful";
        break;
      case 3:
        s = "malicious";
        break;
      case 4:
        s = "nervous";
        break;
      case 5:
        s = "orange";
        break;
      case 6:
        s = "miniature";
        break;
      case 7:
        s = "curious";
        break;
      case 8:
        s = "hospitable";
        break;
      case 9:
        s = "cold";
        break;
    }
    return s;
  }
  
  public static String subjectNoun() {
    Random rand = new Random();
    int i = rand.nextInt(10);
    String s="";
    switch (i) {
      case 0:
        s = "snail";
        break;
      case 1:
        s = "car";
        break;
      case 2:
        s = "plane";
        break;
      case 3:
        s = "friend";
        break;
      case 4:
        s = "dog";
        break;
      case 5:
        s = "giraffe";
        break;
      case 6:
        s = "bee";
        break;
      case 7:
        s = "fish";
        break;
      case 8:
        s = "bird";
        break;
      case 9:
        s = "man";
        break;
    }
    return s;
  }
  
  public static String pastVerb() {
    Random rand = new Random();
    int i = rand.nextInt(10);
    String s="";
    switch (i) {
      case 0:
        s = "launched";
        break;
      case 1:
        s = "held";
        break;
      case 2:
        s = "collected";
        break;
      case 3:
        s = "protected";
        break;
      case 4:
        s = "obtained";
        break;
      case 5:
        s = "stole";
        break;
      case 6:
        s = "melted";
        break;
      case 7:
        s = "followed";
        break;
      case 8:
        s = "cleaned";
        break;
      case 9:
        s = "attacked";
        break;
    }
    return s;
  }
  
  public static String objectNoun() {
    Random rand = new Random();
    int i = rand.nextInt(10);
    String s="";
    switch (i) {
      case 0:
        s = "boat";
        break;
      case 1:
        s = "lettuce";
        break;
      case 2:
        s = "store";
        break;
      case 3:
        s = "toothbrush";
        break;
      case 4:
        s = "bed";
        break;
      case 5:
        s = "table";
        break;
      case 6:
        s = "book";
        break;
      case 7:
        s = "television";
        break;
      case 8:
        s = "blanket";
        break;
      case 9:
        s = "bridge";
        break;
    }
    return s;
  }
  
  public static String thesisSentence() {
    String s = subjectNoun();
    System.out.println("The " + adjective() + ' ' + adjective() + ' ' + s + ' ' + pastVerb() + " the " + adjective() + ' ' + objectNoun() + ". ");
    return s;
  }
  
  public static void actionSentence( String s ) {
    System.out.println("This " + s + " was " + adjective() + " when it " + pastVerb() + " the " + adjective() + ' ' + objectNoun() + ". ");
  }
  
  public static void conclusionSentence( String s ) {
    System.out.println("That " + s + " definitely " + pastVerb() + " that " + objectNoun() + '!');
  }
  
  public static void paragraph() {
    String subject = "";
    Scanner scan = new Scanner(System.in);
    boolean anotherSentence=true;
    String choice="";
    while (anotherSentence) {
      subject = thesisSentence();
      System.out.print("Do you want another sentence? If so, enter 'Yes': ");
      choice=scan.next();
      if ( choice.equals("Yes") ) {
        anotherSentence=true;
      }
      else {
        anotherSentence=false;
      }
    }
    Random rand = new Random();
    for ( int i=0;  i<=rand.nextInt(6); i++ ) {
      actionSentence(subject);
    }
    conclusionSentence(subject);
  }
  
  
}//end class
