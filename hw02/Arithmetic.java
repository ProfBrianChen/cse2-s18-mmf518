//Matthew Firestone
//CSE2 hw02
//06 Feb 2018
//This program prints subtotals and total amounts to be paid for an order of pants, shirts, and belts. 

public class Arithmetic {

  public static void main(String[] args) {//main method
    
  ////Input variables: prices and amounts bought
  //Number of pairs of pants
  int numPants = 3; 
  //Cost per pair of pants
  double pantsPrice = 34.98;

  //Number of sweatshirts
  int numShirts = 2;
  //Cost per shirt
  double shirtPrice = 24.99;

  //Number of belts
  int numBelts = 1;
  //cost per belt
  double beltPrice = 33.99;
    
  //the tax rate
  double paSalesTax = 0.06;

  
    
  //output variables: total costs, total tax, and grand totals
  double costOfPants;   //total cost of pants
  double costOfShirts;   //total cost of shirts
  double costOfBelts;   //total cost of belts
  double taxOnPants;   //total tax of pants
  double taxOnShirts;   //total tax of shirts
  double taxOnBelts;   //total tax of belts
  double totalCost, totalTax, totalPaid; //subtotal, total tax, and grand total
    
  //calculations:    
  costOfPants = numPants*pantsPrice; //calculate total cost of pants
  costOfShirts = numShirts*shirtPrice; //calculate total cost of shirts
  costOfBelts = numBelts*beltPrice; //calculate total tax of belts
    
  taxOnPants = costOfPants*paSalesTax;   //calculate total tax of pants
  taxOnShirts = costOfShirts*paSalesTax;   //calculate total tax of shirts
  taxOnBelts = costOfBelts*paSalesTax;   //calculate total tax of belts
  
  totalCost = costOfPants + costOfShirts + costOfBelts; //calculate pre-tax total of all items
  totalTax = taxOnPants + taxOnShirts + taxOnBelts;     //calculate total of taxes on all items
  totalPaid = totalCost + totalTax;                     //calculate after-tax grand total 
  
  /* Print amounts to screen
     Note: money values are formatted to be cut off at 2 decimal places */
 
  //print pre-tax subtotals by type of item
  System.out.println("The total cost of pants is $" + ((double) (((int) (costOfPants*100))))/100);
  System.out.println("The total cost of shirts is $" + ((double) (((int) (costOfShirts*100))))/100);
  System.out.println("The total cost of belts is $" + ((double) (((int) (costOfBelts*100))))/100);
    
  //print total taxes by type of item
  System.out.println("The total tax on pants is $" + ((double) (((int) (taxOnPants*100))))/100);
  System.out.println("The total tax on shirts is $" + ((double) (((int) (taxOnShirts*100))))/100);
  System.out.println("The total tax on belts is $" + ((double) (((int) (taxOnBelts*100))))/100);
    
  //print totals before and after tax, and amount of tax 
  System.out.println("The total before tax on all items is $" + ((double) (((int) (totalCost*100))))/100);
  System.out.println("The total tax on all items is $" + ((double) (((int) (totalTax*100))))/100);
  System.out.println("The total amount to be paid after tax is $" + ((double) (((int) (totalPaid*100))))/100);
    
  }//end main method 
  
}//end class Arithmetic