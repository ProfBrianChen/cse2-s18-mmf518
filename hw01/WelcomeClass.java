//Matthew Firestone
//CSE2 hw01
//04 Feb 2018
//This program welcomes the user to class by printing a signed welcome message, as well as an introduction of myself. 

public class WelcomeClass {

  public static void main(String[] args) {//main method
    
    //prints formatted welcome message with lehigh ID signature
    System.out.println(
"  -----------\n  | WELCOME |\n  -----------\n  ^  ^  ^  ^  ^  ^\n / \\/ \\/ \\/ \\/ \\/ \\\n<-M--M--F--5--1--8->\n \\ /\\ /\\ /\\ /\\ /\\ /\n  v  v  v  v  v  v");
    
    //prints tweet-length autobiographical statement 
    System.out.println("\nMy name is Matt Firestone and I am a senior at Lehigh University majoring in Finance. I am originally from Lebanon, PA");
  }//end main method
    
}//end class WelcomeClass 