//Matthew Firestone
//CSE2 lab06
//09 March 2018
/* Encrypted X program: 
   
*/ 
import java.util.Scanner;

public class encrypted_x {
  
  // main method 
  public static void main(String[] args) {
    
    Scanner scan = new Scanner (System.in); //create scanner object
    String junkValue="";
    int input=-1;
    
        
    while (input>100 || input<0) {
      System.out.print("Enter the number of stars: ");
      
      if (scan.hasNextInt()) {//waits for user to enter something, checks if it is an integer
       input=scan.nextInt(); //if int, store it as input
      }
      else {//if not an integer
        junkValue=scan.next();//clear scanner value
        while (true) {
          System.out.print("Invalid. Enter a number from 0 to 100 (integer): "); //promts user to try again
          if (scan.hasNextInt()) { //waits for user to enter because the Scanner was cleared
            input=scan.nextInt(); //if int, store it as input
            break; //ends loop when input has a value
          }
          else {//if not an integer
            junkValue=scan.next(); //clear Scanner of incorrect type value
          }
        }
      }
      
      if (input>100 || input <0) {
        System.out.print("Invalid. Must be between 0 and 100. ");
      }
      
    }//ends when input is 0-100
    
    
    for(int i=0; i<=input; i++) {//each is a new row
      
      
      
      for(int j=0; j<=input; j++) {
        if (i==j || i+j==input) {
          System.out.print(' ');
        }
        else {
          System.out.print('*');
        }
        
      }//end inner loop
      System.out.println();
      
      
      
    }//end outer loop
    
    
    
    
    
  }//end main
  
}//end class encrypted_x
      